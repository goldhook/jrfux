<!doctype html>

<html>
<head>
  <meta charset="UTF-8">
  <title>JRF - Content Listing page</title>
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/2.1.2/normalize.css">
  <link rel="stylesheet" href="css/foundation.css" />
  <link rel="stylesheet" href="css/prism.css" type="text/css" />
  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
  <script type="text/javascript" src="https://djyhxgczejc94.cloudfront.net/frameworks/foundation/5.0.3/js/foundation.min.js"></script>
  <!--<script type="text/javascript" src="js/prototype.js"></script>-->
  <!--<script type="text/javascript" src="js/prism.js"></script>-->
  <script>
  $(document).foundation({
    tab: {
      callback : function (tab) {
        console.log(tab);
      }
    }
  });
  </script>
</head> 
<body>
  <div class="container">
    <!--header-->
    <header>
     <div class="row spacer">
      <div class="columns small-12 medium-3 large-3">
        <h1><a href="homepage.html">JRF</a></h1>
      </div>


      <div class="columns small-12 medium-5 large-5">
        <div class="row collapse top">
          <div class="small-10 columns">
            <input type="text" placeholder="Insert search terms here" class="share">
          </div>
          <div class="small-2 columns">
            <span class="postfix radius"><a href="#" class="noticon">Search</a></span>
          </div>
        </div>
      </div>

      <div class="columns small-12 medium-4 large-4">            
        <nav class="top-bar" data-topbar>
          <ul class="title-area">
            <li class="toggle-topbar menu-icon">
              <a href="#"><span>Menu</span></a>
            </li>
          </ul>

          <section class="top-bar-section">
            <ul class="left">
              <li class="divider"></li>
              <li>
                <a class="active" href="landing_about.html">About</a>
              </li>
              <li>
                <a href="landing_projects.html">Projects</a>
              </li> 
              <li>
                <a href="landing_events.html">Events</a>
              </li>
              <li class="divider hide-for-small"></li>
            </ul>
          </section>
        </nav>
      </div>
    </div>        
  </header>

  <!--/header-->
  <!--main-->
  <div class="row spacer">
    <div class="columns small-12 ">
      <h1 class="text-center">We make digital work for you</h1>
    </div>

    <div class="columns small-12 medium-12 large-8">
      <img src="http://placehold.it/640x400">
    </div>

    <div class="columns small-12 medium-12 large-4">
      <p>Sift Digital have been leading the way in the field of digital engagement for over seven years.</p>
      <p>Our mission is simple: to help organisations make the most out of their conversations with members, supporters, advocates and customers.</p>
    </div>
  </div>

  <div class="row spacer">
    <div class="columns small-12 medium-8"> 
      <dl class="tabs" data-tab>
        <dd class="tab-title active"><a href="#panel2-1">Latest News</a></dd>
        <dd class="tab-title"><a href="#panel2-2">Latest Views</a></dd>
        <dd class="tab-title"><a href="#panel2-3">Some faces</a></dd>
        <dd class="tab-title"><a href="#panel2-4">TWB Events</a></dd>
      </dl>

      <div class="tabs-content">
        <div class="content active" id="panel2-1">
          <p>First panel content goes here...</p>
        </div>
        <div class="content" id="panel2-2">
          <p>Second panel content goes here...</p>
        </div>
        <div class="content" id="panel2-3">
          <ul class="small-block-grid-2 medium-block-grid-3">
            <li><img src="http://placehold.it/120x120"></li>
            <li><img src="http://placehold.it/120x120"></li>
            <li><img src="http://placehold.it/120x120"></li>
            <li><img src="http://placehold.it/120x120"></li>
          </ul>
        </div>
        <div class="content" id="panel2-4">
          <p>Fourth panel content goes here...</p>
        </div>
      </div>
    </div>
    <div class="columns small-12 medium-4 large-4"> 
      <h3>Who we work with</h3>
      <ul  class="no-bullet">
        <li>Mind</li>
        <li>Samaritans</li>
        <li>DEC</li>
        <li>World Animal Protection</li>
      </ul>

      <ul class="side-nav">
        <li><a href="landing_portfolio.html">Secondary action(s) <i class="fa fa-users"></i></a></li>
      </ul>     
    </div>
  </div>  

  <div class="row spacer">
    <div class="columns small-12"> 
      <h2 class="text-center">Recent Projects</h2>

      <ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-4">
        <li><a href="#"><img src="http://placehold.it/200x200"></a></li>
        <li><a href="#"><img src="http://placehold.it/200x200"></a></li>
        <li><a href="#"><img src="http://placehold.it/200x200"></a></li>
        <li><a href="#"><img src="http://placehold.it/200x200"></a></li>
        <li><a href="#"><img src="http://placehold.it/200x200"></a></li>
        <li><a href="#"><img src="http://placehold.it/200x200"></a></li>
      </ul>
    </div>
  </div>
  <!--/main-->
  <!--footer-->
  <footer>
    <div class="row spacer"> 
      <div class="columns small-12 medium-4">
        <address>Sift Digital<br />
          Fifth floor<br />
          Up the lift<br />
          The one with the mirrors
        </address>
      </div>
      <div class="columns small-12 medium-4">
        <img src="http://www.siftdigital.co.uk/sites/default/themes/genesis_siftgroups/images/block/twb-logo.png" />
        <h3>Together we're better</h3>
      </div>
      <div class="columns small-12 medium-4">
        <!--share-->
        <div class="panel callout share">
          <div class="row">
            <div class="columns small-12 large-12 medium-12">
              <h3>Social</h3>
              <a class="button tiny" href="#" ><i class="fa fa-facebook"></i></a>
              <a class="button tiny" href="#" ><i class="fa fa-twitter"></i></a>
              <a class="button tiny" href="#" ><i class="fa fa-envelope-o"></i></a>
            </div>
          </div>
        </div>
        <!--/share-->
      </div>
    </div>
  </footer>
  <!--/footer-->
</div>
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation.min.js"></script>
<script>
$(document).foundation();
</script>
</body>
</html>