<header>
 <div class="row spacer">
  <div class="columns small-12 medium-3 large-3">
    <h1><a href="homepage.html">JRF</a></h1>
  </div>


  <div class="columns small-12 medium-5 large-5">
    <div class="large-8 small-9 columns">
      <input type="text" placeholder="Find Stuff">
    </div>
    <div class="large-4 small-3 columns">
      <a href="#" class="alert button expand">Search</a>
    </div>
  </div>

  <div class="columns small-12 medium-4 large-4">            
    <nav class="top-bar" data-topbar>
      <ul class="title-area">
        <li class="toggle-topbar menu-icon">
          <a href="#"><span>Menu</span></a>
        </li>
      </ul>

      <section class="top-bar-section">
        <ul class="left">
          <li class="divider"></li>
          <li>
            <a class="active" href="landing_about.html">About</a>
          </li>
          <li>
            <a href="landing_projects.html">Projects</a>
          </li> 
          <li>
            <a href="landing_events.html">Events</a>
          </li>
          <li class="divider hide-for-small"></li>
        </ul>
      </section>
    </nav>
  </div>
</div>        
</header>
